
const prim_a = "matti";
let prim_b = prim_a;
prim_b = "teppo";
console.log(prim_a);

const reference_variable_a = {name: "matti"};
reference_variable_a = {name: "seppo"}; // not possible
reference_variable_b = reference_variable_a;
reference_variable_b.name = "seppo";
console.log(reference_variable_a);

c = reference_variable_a.name;
c  = "seppo";
console.log(reference_variable_a);
